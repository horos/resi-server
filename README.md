# Resi Web API Platform

This is still in testing

This platform allows you to implement your API on the server side,
and generate a client side with built in intellisense support.

The package ships with PASETO token management, and support for streaming responses from the server.

This is the SERVER version. For client, refer to @horos/resi-server.

## Installation

`npm i -s @horos/resi-server` or
`yarn add @horos/resi-server`

## Server side usage

1. Create 2 folders, one for your api files (the actual functions) and one for your models.
2. Create a model file

```javascript
export class TestModel {
  /**
   *
   * @param {string} a
   */
  constructor(a) {
    this.a = a;
  }
}
```

3. Create an API file

```javascript
import { TestModel } from '../models/testModel';
import { enrich, authorization } from '@horos/resi-server/plugs';
import { createAPIImplementation } from '@horos/resi-server/create-api';

//                                       ⬇ API name
export default createAPIImplementation('test', {
  /**
   * This JSDoc will be kept for intellisense on the client side
   *
   * @param {number} num1
   * @param {number} num2
   * @param {import('@horos/resi-server/dist/create-server').ResiContext} context
   * @returns {Promise<number>}
   */
  test(num1, num2, context) {
    return num1 + num2;
  },

  /**
   * @returns {Promise<import('../models/testModel').TestModel>}
   */

  //         ⬇ Use the enrich function to plug features to your funtion
  shogi: enrich(async function (context) {
    return new TestModel('banana');
    // ⬇ In this case authorization is plugged, which means only authorized clients can invoke this handler
  }, authorization),
});
```

## ** New in 0.2.0 **

It is now possible to plug functions with decorators.

```javascript
export default createAPIImplementation(
  'test',
  // ⬇ To use decorators, you must use a class and not an object literal
  class {
    /**
     *
     * @param {string} searchTerm
     * @param {boolean} onlyCountrySpecific
     * @param {import('@horos/resi-server/dist/create-server').ResiContext} context
     * @returns {Promise<{languages: Language[]}>}
     */
    // ⬇ Use a plug as a decorator instead of using the "enrich" function
    @httpDelete
    searchLanguages(searchTerm, onlyCountrySpecific, context) {
      return `${searchTerm} ${onlyCountrySpecific} 3`;
    }
  },
);
```

4. Security object is needed for authorizing incoming requests. 3 Keys are required in total - Public/Private key pair, and a secret key.

You can generate them like this by adding this command to your package.json scripts:

`"generate-security-keys": "create-key-set ./security-keys"`

4. Finally create your RESI API using your API and models directories, and your security files.

- In production, remember to specify the allowed origins for the CORS headers.
- By default, building the full API is not allowed. When building on the client, make sure to specify APIs, or disable this restriction (see example below).

```javascript
import { createServerFromResiDir } from '@horos/resi-server';

const securityPaths = {
  publicKey: './security-keys/public',
  privateKey: './security-keys/private',
  secret: './security-keys/secret',
};

async function init() {
  await createServerFromResiDir(
    path.resolve('./src/resi-server/models'),
    path.resolve('./src/resi-server/apis'),
    'dist',
    {
      securityPaths,
      allowedOrigins: ['www.mydomain.com', 'undefined'], // include undefined to support clients who don't rely on cors (i.e. apps). You can also specify a string (i.e. '*' or 'www.mydomain.com')
      allowBuildingFullAPI: false, // using this, a developer wishing to download the API specs is required to specify APIs for download. Use this hide APIs from clients who arent going to use them
      port: 9876,
    },
  );
}
```

## Client side usage

1. Install @horos/resi-client on your client as well
2. Make sure the server is running with NODE_ENV set to "development"
3. Add a script to your package.json
   `"build-client": "build-client http://localhost:9876 ./src/resi-client" API1 API2`
   Replace the "http://localhost:9876" with the url of your server, and "src/resi-client" with any path you wish.
   If you allowed building the entire API, you don't need to specify APIs at the end of the command.
4. Execute the new script. This should create a local API, identical to the one on your server
5. Test your client

```javascript
import { makeResiClient } from './resi-client/apis';

export const resi = makeResiClient('http://localhost');
```

Type "resi." and you should see intellisense auto compeleting according to your server definition.

Execute any function, and it should invoke its equivalent on the server.
