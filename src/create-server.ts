import express, { Router, Express, Request, NextFunction, Response, Handler } from 'express';
import { Server } from 'http';
import rateLimit from 'express-rate-limit';
import morgan from 'morgan';
import path from 'path';
import cors from 'cors';

type CustomOrigin = (requestOrigin: string | undefined, callback: (err: Error | null, allow?: boolean) => void) => void;

import crypto from 'crypto';
import { promises as fs } from 'fs';

import { addAPIToRouter } from './mapAPIToRouter';

import { buildClientFileCommands } from './build-client';

import { serverRunningMessage } from './server-running';
import { ResiToken } from './security/ResiToken';
import { KeyFile, makeAuthorizationMiddleware, makeRoleAuthorizationMiddleware } from './security';
import { ResiAPIImplementation, ResiHandler, RESI_ROUTE } from '@horos/resi-common/types-consts';
import { enrich } from '@horos/resi-common/plugs';
import { customRequestBody, streamResponse } from './lib/plugs';
import { BUILD_CLIENT_API } from '@horos/resi-common/client-builder';
import { ResiSecurity, ResiSecurityPaths } from './lib/types-consts';
import { mergeOptions } from './lib/utils';

export const reqToLog = (req: Request) => ({
  headers: req.headers,
  body: req.body,
  query: req.query,
  path: req.path,
  accepted: req.accepted
});

export type ResiContext<TToken = ResiToken> = {
  req: Request;
  res: Response;
  writeStream?: (chunk: Buffer | string) => void;
  token: TToken;
  resiOptions: CreateServerOptions;
};

// Enable if you're behind a reverse proxy (Heroku, Bluemix, AWS ELB, Nginx, etc)
// see https://expressjs.com/en/guide/behind-proxies.html
// app.set('trust proxy', 1);

//  apply to all requests

function createSuccessAndErrorHandlers(app: Express, logger: any) {
  // development error handler
  // will print stacktrace
  if (app.get('env') === 'development') {
    app.use(function (err: any, req: Request, res: Response, next: NextFunction) {
      if (res.headersSent) {
        return next(err);
      }

      if (err.sql) err.message += ' SQL: ' + err.sql;

      res.status(err.status || 500).send({
        message: err,
        error: err.message
      });
    });
  }

  // production error handler
  // no stacktraces leaked to user
  app.use(function (err: any, req: Request, res: Response & { body?: object }, next: NextFunction) {
    if (res.headersSent) {
      return next(err);
    }

    logger.error('GENERAL ERROR REPORT', {
      error: err,
      req: reqToLog(req),
      res: { body: res.body }
    });

    res.status(err.status || 500).send({
      message: {},
      error: err.message
    });
  });
}

function createOriginOptionForCors(options: CreateServerOptions) {
  if (!options.allowedOrigins) return '*';

  if (options.allowedOrigins.toString() === options.allowedOrigins) return options.allowedOrigins;

  if (options.allowedOrigins && options.allowedOrigins.length > 0) {
    const allowedOriginsMap = Object.assign({}, ...(options.allowedOrigins as string[]).map((ao) => ({ [ao]: true })));
    const retval: CustomOrigin = (origin, callback) => {
      if (origin === undefined && allowedOriginsMap.undefined) callback(null, true);
      else if (origin !== undefined && allowedOriginsMap[origin]) callback(null, true);
      else callback(new Error('Rejected by CORS'));
    };
    return retval;
  }
}

export type CreateServerOptions = {
  logger: Console;
  apiPrefix: string;
  port: number;
  bodyLimit: string;
  allowedOrigins: string | string[];
  security: ResiSecurity;
  securityPaths: ResiSecurityPaths;
  allowBuildingFullAPI: boolean;
  authorizationMiddleware: Handler;
  makeRoleAuthorizationMiddleware: (roles: number[]) => Handler;
  hookSetup: (
    app: Express,
    router: Router,
    apiImplementation: ResiAPIImplementation,
    options: CreateServerOptions
  ) => Promise<void>;
  setup: (
    app: Express,
    router: Router,
    apiImplementation: ResiAPIImplementation,
    options: CreateServerOptions
  ) => Promise<void>;
  hookStart: (app: Express, router: Router, options: CreateServerOptions) => Promise<void>;
  start: (app: Express, router: Router, options: CreateServerOptions) => Promise<Server>;
  hookSetRoutes: (
    app: Express,
    router: Router,
    apiImplementation: ResiAPIImplementation,
    options: CreateServerOptions
  ) => Promise<void>;
  setRoutes: (
    app: Express,
    router: Router,
    apiImplementation: ResiAPIImplementation,
    options: CreateServerOptions
  ) => Promise<void>;
  noLogRoutes: string[];
};

export type CreateServerUserOptions = {
  [key in keyof CreateServerOptions]?: CreateServerOptions[key];
};

const defaultOptions: CreateServerOptions = {
  logger: console,
  apiPrefix: RESI_ROUTE,
  port: 80,
  bodyLimit: '16mb',
  allowedOrigins: '*',
  security: {
    publicKey: Buffer.from(''),
    privateKey: Buffer.from(''),
    secret: Buffer.from('')
  },
  securityPaths: {
    publicKey: '',
    privateKey: '',
    secret: ''
  },
  allowBuildingFullAPI: true,
  noLogRoutes: [],
  authorizationMiddleware: (req, res, next) => next(),
  makeRoleAuthorizationMiddleware(roles) {
    return this.authorizationMiddleware;
  },
  async hookSetup(app: Express, router: Router) {
    return;
  },
  /**
   *
   * @param {import('express').Express} app
   */
  async setup(
    app: Express,
    router: Router,
    resiAPIImplementation: ResiAPIImplementation,
    options: CreateServerOptions
  ) {
    await options.hookSetup(app, router, resiAPIImplementation, options);
    const limiter = rateLimit({
      windowMs: 15 * 60 * 1000, // 15 minutes
      max: 100 // limit each IP to 100 requests per windowMs
    });

    // app.use(limiter, morgan('dev'), cors(), bodyParser.json({ limit: options.bodyLimit }));
    app.use(
      morgan('dev', {
        skip(req, res) {
          if (options.noLogRoutes.length === 0) return false;
          else {
            const noLogPath = options.noLogRoutes.find((r) => req.path.endsWith(r));
            return Boolean(noLogPath);
          }
        }
      }),
      cors({
        origin: createOriginOptionForCors(options)
      }),
      express.json({ limit: options.bodyLimit })
    );

    await options.setRoutes(app, router, resiAPIImplementation, options);
    app.use(`/${options.apiPrefix}`, router);

    createSuccessAndErrorHandlers(app, options.logger);
  },

  async hookStart(app, router, options) {
    return;
  },

  start(app: Express, router: Router, options: CreateServerOptions): Promise<Server> {
    return new Promise((resolve, reject) => {
      try {
        const server = app.listen(options.port, () => {
          options.logger.info(`Listening on port ${options.port}`);
          resolve(server);
        });
      } catch (e) {
        reject(e);
      }
    });
  },
  async hookSetRoutes(app, router, apiImplementation) {
    return;
  },

  async setRoutes(app, router, apiImplementation, options) {
    await options.hookSetRoutes(app, router, apiImplementation, options);
    addAPIToRouter(router, apiImplementation, options);
  }
};

export async function createExpressApp(resiAPIImplementation: ResiAPIImplementation, options = defaultOptions) {
  const mergedOptions = mergeOptions(options, defaultOptions);

  if (!options.security && options.securityPaths) {
    mergedOptions.security = (await KeyFile.resolveKeyFiles(options.securityPaths)) as ResiSecurity;
  }

  if (mergedOptions.security) {
    const publicKey = crypto.createPublicKey(mergedOptions.security.publicKey);
    const secret = crypto.createSecretKey(mergedOptions.security.secret);
    mergedOptions.authorizationMiddleware = makeAuthorizationMiddleware(publicKey, secret, options.logger);
    mergedOptions.makeRoleAuthorizationMiddleware = (roles: number[]) => {
      return makeRoleAuthorizationMiddleware(roles, publicKey, secret, options.logger);
    };
  }
  const { setup } = mergedOptions;

  const app = express();
  const router = Router();

  await setup(app, router, resiAPIImplementation, mergedOptions);
  return { mergedOptions, app, router };
}

export async function createServer(resiAPIImplementation: ResiAPIImplementation, options = defaultOptions) {
  const { mergedOptions, app, router } = await createExpressApp(resiAPIImplementation, options);
  const { start, logger } = mergedOptions;
  const server = await start(app, router, mergedOptions);
  logger.info(serverRunningMessage(mergedOptions, resiAPIImplementation));
  return server;
}

async function getPathsFromDir(dir: string) {
  const files = await fs.readdir(dir);
  return files.filter((f) => false === f.includes('js.map')).map((f) => path.join(dir, f));
}

async function createResiAPIImplementationFromResiDir(
  modelsDirectory: string,
  apisDirectory: string,
  distDir = 'src',
  options = defaultOptions
) {
  const apiFiles = await getPathsFromDir(apisDirectory);
  const modelFiles = await getPathsFromDir(modelsDirectory);
  const resiAPIImplementation: ResiAPIImplementation = Object.assign(
    {},
    ...apiFiles.map((file) => {
      let filePath = file;
      if (false === file.includes(distDir)) {
        filePath = filePath.replace('src', distDir);
      }
      const impl = require(filePath).default;
      return { [impl.name]: impl };
    })
  );

  if (process.env.NODE_ENV === 'development') {
    options.logger.info('Adding handler for client builder');
    const buildImpl = (body: any, context: ResiContext) => {
      const { requestedApis } = body;
      if (
        (false === options.allowBuildingFullAPI && !requestedApis) ||
        (false === options.allowBuildingFullAPI && requestedApis.length === 0)
      ) {
        throw new Error('Must specify APIs');
      }

      if (context.writeStream) {
        const { writeStream } = context;
        buildClientFileCommands(
          resiAPIImplementation,
          apiFiles,
          modelFiles,
          requestedApis || [],
          distDir,
          (createFileMessage) => writeStream(JSON.stringify(createFileMessage))
        );
      } else throw new Error('No writeStream in Context!');
    };
    const build: ResiHandler = enrich<typeof buildImpl>(buildImpl, streamResponse, customRequestBody);
    resiAPIImplementation[BUILD_CLIENT_API] = {
      build,
      name: BUILD_CLIENT_API
    };
  }

  return resiAPIImplementation;
}

export async function createExpressAppFromResiDir(
  modelsDirectory: string,
  apisDirectory: string,
  distDir = 'src',
  options = defaultOptions
) {
  const resiAPIImplementation = await createResiAPIImplementationFromResiDir(
    modelsDirectory,
    apisDirectory,
    distDir,
    options
  );
  return createExpressApp(resiAPIImplementation, options);
}

export async function createServerFromResiDir(
  modelsDirectory: string,
  apisDirectory: string,
  distDir = 'src',
  options = defaultOptions
) {
  const resiAPIImplementation = await createResiAPIImplementationFromResiDir(
    modelsDirectory,
    apisDirectory,
    distDir,
    options
  );
  return await createServer(resiAPIImplementation, options);
}

/**
 * @returns {import('./mapAPIToRouter').Context}
 */
export function getContext(a = arguments) {
  const args = getContext.caller.arguments;
  return args[args.length - 1];
}
