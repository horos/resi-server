import fs from 'fs';
import path from 'path';
import { Parser } from 'acorn';
import { ResiAPIImplementation, ResiHandler } from '@horos/resi-common/types-consts';
import { CreateFileMessage, PlugsMap } from '@horos/resi-common/client-builder';
import { PLUGS, readPlugs, SERVER_ONLY_PLUGS } from './lib/plugs';

const REMOVE_PLUGS = Object.keys(SERVER_ONLY_PLUGS);

const FUNCTION_TYPES = ['CallExpression', 'FunctionExpression'];

export function buildClientFileCommands(
  resiAPIImplementation: ResiAPIImplementation,
  apiFiles: string[],
  modelFiles: string[],
  requestedApis: string[],
  distDir: string,
  callback: (buf: CreateFileMessage) => void
) {
  const apiFilesForClient =
    requestedApis.length === 0
      ? apiFiles
      : apiFiles.filter((apifile) => {
          const filename = path.basename(apifile, '.js');
          return requestedApis.includes(filename);
        });

  apiFilesForClient.forEach((apiFile) => {
    const content = buildAPIFile(resiAPIImplementation, apiFile, distDir);
    if (content) callback(content);
  });

  modelFiles.forEach((modelFile) =>
    callback(new CreateFileMessage(path.basename(modelFile), fs.readFileSync(modelFile).toString(), 'models'))
  );

  return {};
}

function buildAPIFile(resiAPIImplementation: ResiAPIImplementation, apiFile: string, distDir: string) {
  console.log('Building: ', apiFile);
  let filePath = apiFile;
  if (false === apiFile.includes(distDir)) {
    filePath = filePath.replace('src', distDir);
  }
  const fileImpl = require(filePath).default;
  const impl = resiAPIImplementation[fileImpl.name];
  const apiPlugs = readPlugs(impl);
  Object.keys(apiPlugs).forEach((plug) => {
    if (REMOVE_PLUGS.includes(plug)) delete apiPlugs[plug];
    if (plug === PLUGS.roleAuthorization) {
      delete apiPlugs[plug];
      apiPlugs[PLUGS.withAuthorization] = true;
    }
  });

  let content = fs.readFileSync(apiFile).toString();
  content = removeUnwantedLines(content);

  const parse: acorn.Node & { body: any[] } = Parser.parse(content, {
    sourceType: 'module',
    ecmaVersion: 'latest'
  }) as acorn.Node & {
    body: any[];
  };

  let properties: any[] = [];

  const objNode = parse.body.find((n) => {
    if (!n.declaration || !n.declaration.callee || n.declaration.callee.name !== 'createAPIImplementation')
      return false;

    properties = n.declaration.arguments[1].properties || n.declaration.arguments[1].body.body;
    return true;
  });

  if (!objNode || !properties) return null;

  const replacements = [];
  const removals = [];
  const plugsMap: PlugsMap = {
    API: apiPlugs
  };

  for (let i = 0; i < properties.length; i++) {
    const n = properties[i];
    const funcImpl = impl[n.key.name as string] as ResiHandler;
    if (funcImpl instanceof Function) {
      const functionBody = content.substring(n.start, n.end);
      const replacementObject = { functionBody, name: n.key.name, funcImpl, params: undefined };
      if (FUNCTION_TYPES.includes(n.value.type)) {
        const rawParams = n.value.arguments ? n.value.arguments[0].params : n.value.params;
        replacementObject.params = rawParams.map((a: any) => a.name).filter((p: string) => p !== 'context');
      }
      replacements.push(replacementObject);
      const plugs = readPlugs(impl[n.key.name]);
      plugsMap[n.key.name] = plugs;
    }
  }

  const charsForRemoval = [' ', '\t', '\n', '\r', ','];
  if (objNode.declaration.arguments.length > 2) {
    for (const argument of objNode.declaration.arguments.slice(2)) {
      let start = argument.start;
      while (charsForRemoval.includes(content[start - 1])) start--;
      removals.push(content.substring(start, argument.end));
    }
  }

  replacements.forEach(({ functionBody, name, funcImpl, params }) => {
    if (params) {
      funcImpl.__params = params;
    }
    const finalParams = funcImpl.__params || [];
    content = content.replace(functionBody, `${name}(${finalParams.join(', ')}){}`);
  });

  removals.forEach((r) => {
    content = content.replace(r, '');
  });

  content = replaceRoleAuthorization(content);

  return new CreateFileMessage(path.basename(apiFile), content, 'APIs', plugsMap, impl.name);
}

function replaceRoleAuthorization(content: string) {
  const indexOf = content.indexOf('roleAuthorization');
  if (indexOf > 0) {
    const closingParenthesis = content.indexOf(')', indexOf);
    return `${content.substring(0, indexOf)}authorization${content.substring(closingParenthesis + 1)}`;
  }
  return content;
}

function isImportEnding(row: string) {
  let idx = row.length - 1;
  if (row.endsWith('\r')) {
    idx--;
  }
  const c = row[idx];
  return c === ';' || c === "'" || c === '"';
}

/**
 * Strips API source code from non-resi imports, decorators
 * @param content : source code;
 * @returns
 */
function removeUnwantedLines(content: string) {
  console.log('Stripping');
  let multiLineImport: string[] = [];
  let openParenthesisCount = 0;
  let closedParenthesisesCount = 0;

  const handleParenthesis = (row: string) => {
    openParenthesisCount += row.split('(').length - 1;
    closedParenthesisesCount += row.split(')').length - 1;

    if (openParenthesisCount === closedParenthesisesCount) {
      openParenthesisCount = closedParenthesisesCount = 0;
    }
  };

  const stripped = content
    .replace('\r\n', '\n')
    .split('\n')
    .map((row) => {
      if (row.startsWith('import') && false === isImportEnding(row)) {
        multiLineImport.push(row);
        return null;
      }

      if (multiLineImport.length > 0) {
        multiLineImport.push(row);
        if (isImportEnding(row)) {
          const newRow = multiLineImport.join('');
          multiLineImport = [];
          return newRow;
        } else return null;
      }

      // decorators
      if ((row.startsWith('@') || row.includes(' @') || row.includes('\t@')) && false === row.includes('* @')) {
        handleParenthesis(row);
        return null;
      }

      if (openParenthesisCount > 0) {
        handleParenthesis(row);
        return null;
      }

      if (row.includes('* @param') && row.includes('context')) {
        return null;
      }

      return row;
    })
    .map((row) => {
      if (!row) return row;
      if (row.includes('import { createAPIImplementation }') && row.includes('@horos/resi-server/create-api')) {
        return "import { createAPIImplementation } from '@horos/resi-client/create-api';";
      }
      const rowHasRequireOrImport = row.includes('require') || row.includes('import');
      const keep =
        false === rowHasRequireOrImport ||
        row.includes('models') ||
        (row.includes('@horos/resi') && false === (row.includes('server') || row.includes('security')));
      if (!keep) {
        process.stdout.write(row);
        return null;
      }
      return row;
    })
    .filter((i) => i)
    .join('\n');
  console.log();
  console.log('New Content');
  console.log(stripped);
  return stripped;
}
