import { checkPlug, getHttpMethod } from '@horos/resi-common/plugs';
import { ResiAPIImplementation, ResiHandler } from '@horos/resi-common/types-consts';
import { Express, Handler, NextFunction, Request, Response, Router } from 'express';
import { iterateFunctionsAndParams } from './lib/function-parsing';
import { ResiSecurity } from './lib/types-consts';
import { mergeOptions } from './lib/utils';
import { CreateServerOptions, CreateServerUserOptions } from './create-server';
import { GetKeyFunction, PLUGS } from './lib/plugs';
import { makeDecryptPayloadMiddleware, makeVerifyPayloadMiddleware } from './security';
import { RESI_CONTEXT, RESI_TOKEN } from './lib/consts';

const securityDefault: ResiSecurity = {
  privateKey: Buffer.from(''),
  publicKey: Buffer.from(''),
  secret: Buffer.from('')
};
export const defaultOptions = {
  resBodyHandler(resBody: object, res: Response, next: NextFunction) {
    if (resBody) {
      res.send(JSON.stringify(resBody));
    } else res.end();
    if (next) {
      next();
    }
  },

  errorHandler(error: any, res: Response, next: NextFunction) {
    if (next) next(error);
    else res.send(error);
  },

  security: securityDefault,
  logger: console
};

export type AddAPIToRouterOptions = {
  [key in keyof typeof defaultOptions]?: typeof defaultOptions[key];
};

export type AddAPIToRouterOptionsMerged = {
  [key in keyof typeof defaultOptions]: typeof defaultOptions[key];
};

const getArg = (param: string, req: Request) => req.body[param];
const makeGetArg = (req: Request) => (param: string) => getArg(param, req);

function enrichContext(func: any, key: string, value: any) {
  func[key] = value;
}

function makeContextMiddleware(optionsFinal: AddAPIToRouterOptionsMerged & CreateServerOptions) {
  return (req: Request, res: Response, next: NextFunction) => {
    const context = { token: (req as any)[RESI_TOKEN], resiOptions: optionsFinal };

    enrichContext(context, 'req', req);
    enrichContext(context, 'res', res);

    (req as any)[RESI_CONTEXT] = context;
    next();
  };
}

export function addAPIToRouter(
  router: Router,
  resiAPIImplementation: ResiAPIImplementation,
  options: AddAPIToRouterOptions & CreateServerUserOptions = defaultOptions
) {
  const optionsFinal: AddAPIToRouterOptionsMerged & CreateServerOptions = mergeOptions(options, defaultOptions);

  let { logger } = optionsFinal;
  logger = logger || console;

  // logger.log('apiImplementation', apiImplementation);
  for (const apiName in resiAPIImplementation) {
    logger.debug(`API ${apiName}`);

    const api = resiAPIImplementation[apiName];
    const apiAuthorization = checkPlug(api, PLUGS.withAuthorization);
    const apiRoles = (api.__authorized_roles as unknown) as number[];
    const apiHasRolesAuth = checkPlug(api, PLUGS.roleAuthorization);
    const apiHasPrepend = checkPlug(api, PLUGS.prependMiddleware);
    const apiHasAppend = checkPlug(api, PLUGS.appendMiddleware);
    const apiHasNoLog = checkPlug(api, PLUGS.noDefaultLog);
    const apiSignedRequest = checkPlug(api, PLUGS.signedRequest) as undefined | GetKeyFunction<any>;
    const apiEncryptedRequest = checkPlug(api, PLUGS.encryptedRequest) as undefined | GetKeyFunction<any>;

    const apiPath = '/' + apiName;

    let apiRouter = Router();
    router.use(apiPath, apiRouter);

    if (apiHasRolesAuth && optionsFinal.security && optionsFinal.makeRoleAuthorizationMiddleware) {
      apiRouter.use(optionsFinal.makeRoleAuthorizationMiddleware(apiRoles));
      logger.debug(`\t role on API (${apiRoles.join(', ')})`);
    } else if (apiAuthorization && optionsFinal.security && optionsFinal.authorizationMiddleware) {
      apiRouter.use(optionsFinal.authorizationMiddleware);
      logger.debug(`\t auth on API`);
    }

    apiRouter.use(makeContextMiddleware(optionsFinal));

    for (const { func, params } of iterateFunctionsAndParams(resiAPIImplementation[apiName])) {
      const path = '/' + func;
      logger.debug(`\tfunc ${func}`);
      const funcImpl = api[func] as ResiHandler;
      funcImpl.__params = params;
      const httpAction = getHttpMethod(funcImpl);

      const handlers: Handler[] = [];
      const addHandler = (handler: Handler) => handlers.push(handler);

      if (checkPlug(funcImpl, PLUGS.roleAuthorization) && optionsFinal.makeRoleAuthorizationMiddleware) {
        logger.debug(`\t\t role on ${httpAction} (${funcImpl.__authorized_roles.join(', ')})`, { path, httpAction });

        handlers.push(optionsFinal.makeRoleAuthorizationMiddleware(funcImpl.__authorized_roles));
      } else if (
        checkPlug(funcImpl, PLUGS.withAuthorization) &&
        optionsFinal.security &&
        optionsFinal.authorizationMiddleware
      ) {
        logger.debug(`\t\tauth on ${httpAction}`);
        handlers.push(optionsFinal.authorizationMiddleware);
      }

      if (apiSignedRequest) {
        handlers.push(makeVerifyPayloadMiddleware(apiSignedRequest, options.logger));
      }

      const funcSignedRequest = checkPlug(funcImpl, PLUGS.signedRequest);
      if (funcSignedRequest) {
        handlers.push(makeVerifyPayloadMiddleware(funcSignedRequest, options.logger));
      }

      if (apiEncryptedRequest) {
        handlers.push(makeDecryptPayloadMiddleware(apiEncryptedRequest, options.logger));
      }

      const funcEncryptedRequest = checkPlug(funcImpl, PLUGS.encryptedRequest);
      if (funcEncryptedRequest) {
        handlers.push(makeVerifyPayloadMiddleware(funcEncryptedRequest, options.logger));
      }

      if (apiHasPrepend && api.__prepend_middleware_handlers) {
        logger.debug(`\t\t prepending API middleware`);
        ((api.__prepend_middleware_handlers as unknown) as Handler[]).forEach((h) => handlers.push(h));
      }

      if (checkPlug(funcImpl, PLUGS.prependMiddleware)) {
        logger.debug(`\t\t prepending func middleware`);
        if (funcImpl.__prepend_middleware_handlers) funcImpl.__prepend_middleware_handlers.forEach(addHandler);
      }

      if (apiHasNoLog || checkPlug(funcImpl, PLUGS.noDefaultLog)) {
        if (!options.noLogRoutes) options.noLogRoutes = [];
        options.noLogRoutes.push(path);
      }

      handlers.push(function (req, res, next) {
        // logger.debug('INCOMING', { path });

        const args = checkPlug(funcImpl, PLUGS.customRequestBody) ? [req.body] : req.body.args || [];

        const context = (req as any)[RESI_CONTEXT];
        if (checkPlug(funcImpl, PLUGS.streamResponse))
          enrichContext(context, 'writeStream', (chunk: string | Buffer) => res.write(chunk));

        try {
          args.push(context);
          const resOrPromise = funcImpl.apply(api, args);
          if (!resOrPromise) {
            optionsFinal.resBodyHandler(resOrPromise, res, next);
          } else if (resOrPromise.then) {
            resOrPromise
              .then((responseBody: object) => optionsFinal.resBodyHandler(responseBody, res, next))
              .catch((error: any) => optionsFinal.errorHandler(error, res, next));
          } else {
            optionsFinal.resBodyHandler(resOrPromise, res, next);
          }
        } catch (error) {
          optionsFinal.errorHandler(error, res, next);
        }
      });

      if (apiHasAppend || checkPlug(funcImpl, PLUGS.appendMiddleware)) {
        if (api.__append_middleware_handlers)
          ((api.__append_middleware_handlers as unknown) as Handler[]).forEach(addHandler);

        if (funcImpl.__append_middleware_handlers) funcImpl.__append_middleware_handlers.forEach(addHandler);
      }

      apiRouter[httpAction](path, ...handlers);
    }
  }
}
