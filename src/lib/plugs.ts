import { Handler } from 'express';
import { checkPlug, enrich, PLUGS as COMMON_PLUGS } from '@horos/resi-common/plugs';
import { ResiContext } from '../create-server';

export { enrich };

export const SERVER_ONLY_PLUGS = {
  prependMiddleware: '__prepend_middleware',
  appendMiddleware: '__append_middleware',
  noDefaultLog: '__no_defaut_log'
};

export const PLUGS = {
  ...COMMON_PLUGS,
  ...SERVER_ONLY_PLUGS
};

const plugFields = Object.values(PLUGS);

function setPlugTargetField(target: any, key: string, value: any) {
  if (target.descriptor) {
    target.descriptor.value[key] = value;
    return target.descriptor.value;
  } else {
    target[key] = value;
    return target;
  }
}

function defaultPlugAction<T>(target: T, plugName: string) {
  setPlugTargetField(target, plugName, true);
  return target;
}

const makeDefaultPlugAction = (plugName: string) => {
  const func = <T>(target: T) => defaultPlugAction(target, plugName);
  return func;
};

export function readPlugs(func: any) {
  const plugs: { [plug: string]: boolean } = {};
  plugFields.forEach((pf) => {
    if (func[pf]) {
      plugs[pf] = Boolean(func[pf]);
    }
  });
  return plugs;
}

export const streamResponse = makeDefaultPlugAction(PLUGS.streamResponse);
export const authorization = makeDefaultPlugAction(PLUGS.withAuthorization);
export const httpGet = makeDefaultPlugAction(PLUGS.httpGet);
export const httpPut = makeDefaultPlugAction(PLUGS.httpPut);
export const httpDelete = makeDefaultPlugAction(PLUGS.httpDelete);
export const customRequestBody = makeDefaultPlugAction(PLUGS.customRequestBody);
export const noDefaultLog = makeDefaultPlugAction(PLUGS.noDefaultLog);

export const prependMiddleware = (...handlers: Handler[]) => (target: any) => {
  setPlugTargetField(target, '__prepend_middleware_handlers', handlers);
  return defaultPlugAction(target, PLUGS.prependMiddleware);
};

export const appendMiddleware = (...handlers: Handler[]) => (target: any) => {
  setPlugTargetField(target, '__append_middleware_handlers', handlers);
  return defaultPlugAction(target, PLUGS.appendMiddleware);
};

export const customHeader = (headersObj: Object) => (target: any) => {
  setPlugTargetField(target, PLUGS.customHeaders, headersObj);
  return target;
};

export const roleAuthorization = (...roles: number[]) => (target: any) => {
  if (!checkPlug(target, PLUGS.withAuthorization)) {
    authorization(target);
  }

  setPlugTargetField(target, '__authorized_roles', roles);
  return defaultPlugAction(target, PLUGS.roleAuthorization);
};

const defaultCompareBy = (body: any) => JSON.stringify(body);
const keysCompareBy = (body: any, compareBy: string[]) => JSON.stringify(compareBy.map((key) => body[key]));

const cachePlug = (
  plug: string,
  timeoutSeconds: number,
  compareBy: string[] | ((body: Object) => string) = defaultCompareBy
) => (target: any) => {
  const realTarget = setPlugTargetField(target, '__resi_cache', { timeoutSeconds });
  if (false === compareBy instanceof Function) {
    realTarget.__resi_cache.compareBy = compareBy;
  }
  return defaultPlugAction(target, plug);
};

export const serverCache = (
  timeoutSeconds: number = 60 * 15,
  compareBy: string[] | ((body: Object) => string) = defaultCompareBy
) => (target: any) => {
  return cachePlug(PLUGS.cacheServer, timeoutSeconds, compareBy)(target);
};

export const clientCache = (
  timeoutSeconds: number = 60 * 15,
  compareBy: string[] | ((body: Object) => string) = defaultCompareBy
) => (target: any) => {
  return cachePlug(PLUGS.cacheClient, timeoutSeconds, compareBy)(target);
};

export type GetKeyFunction<T> = (context: ResiContext<T>) => Promise<string> | string;

export function signedRequest<T>(getPublicKey: (context: ResiContext<T>) => Promise<string> | string) {
  return (target: any) => {
    setPlugTargetField(target, PLUGS.signedRequest, getPublicKey);
    return target;
  };
}

export function encryptedRequest<T>(getSecretKey: (context: ResiContext<T>) => Promise<string> | string) {
  return (target: any) => {
    setPlugTargetField(target, PLUGS.encryptedRequest, getSecretKey);
    return target;
  };
}
