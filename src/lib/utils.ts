import { ResiAPIImplementation, ResiHandler } from '@horos/resi-common/types-consts';
import { PLUGS } from './plugs';

export function mergeOptions<T, TMerged = T>(options: T, defaultOptions: T) {
  const finalOptions: any = !options
    ? defaultOptions
    : options === defaultOptions
    ? options
    : Object.assign({}, defaultOptions, options);
  Object.keys(finalOptions).forEach((key) => {
    if (finalOptions[key] instanceof Function) {
      finalOptions[key] = finalOptions[key].bind(finalOptions);
    }
  });
  return finalOptions as TMerged;
}

export function describeAPI(resiAPIImplementation: ResiAPIImplementation, tabPadding = '') {
  const reversedPlugsMap = Object.assign({}, ...Object.entries(PLUGS).map(([key, value]) => ({ [value]: key })));
  const lines: string[] = [];
  const columnWidth = 20;
  const defaultColumn = new Array(columnWidth).fill(' ');
  const columnify = (text: string) => text + defaultColumn.slice(Math.min(text.length, columnWidth - 1)).join('');
  Object.keys(resiAPIImplementation).forEach((api) => {
    const plugs: string[] = [];
    const functions: string[] = [];
    Object.keys(resiAPIImplementation[api]).forEach((funcOrPlug) => {
      if (funcOrPlug === 'name') return;

      if (funcOrPlug.startsWith('__')) {
        const plugKey = reversedPlugsMap[funcOrPlug];
        if (plugKey) plugs.push(plugKey);
      } else {
        const functionPlugs = Object.keys(resiAPIImplementation[api][funcOrPlug])
          .filter((f) => f.startsWith('__') && f !== '__params')
          .map((p) => reversedPlugsMap[p]);
        const params =
          (resiAPIImplementation[api][funcOrPlug] &&
            (resiAPIImplementation[api][funcOrPlug] as ResiHandler).__params) ||
          [];

        functions.push(
          `${tabPadding}\t${columnify(funcOrPlug)}  params: ${columnify(params.join(', '))}${
            functionPlugs.length > 0 ? `  plugs: ${functionPlugs.join(', ')}` : ''
          }`
        );
      }
    });

    lines.push(`${tabPadding} API ${columnify(api)}`);
    if (plugs.length > 0) {
      lines.push(`${tabPadding}\tplugs: ${plugs.join(', ')}`);
    }
    lines.push(`${tabPadding}\tfunctions:`);
    lines.push(...functions);
  });
  return lines.join('\n');
}
