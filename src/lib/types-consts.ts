export type ResiHandler = Function & { [key: string]: boolean | any; __params?: string[] };

export type ResiSecurityKeyType = 'privateKey' | 'publicKey' | 'secret';
export type ResiSecurity = {
  [key in ResiSecurityKeyType]: Buffer;
};
export type ResiSecurityPaths = {
  [key in ResiSecurityKeyType]: string;
};
