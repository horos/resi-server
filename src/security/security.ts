import { KeyObject } from 'crypto';
import { NextFunction, Request, Response } from 'express';
import { ResiToken } from './ResiToken';
import { KeyFile } from './KeyFile';
import { PasswordUtils } from './passwords';
import { AUTH_TOKEN_FIELD, BODY_FIELD_ENCRYPTED, BODY_FIELD_SIGNED } from '@horos/resi-common/types-consts';
import { RESI_CONTEXT, RESI_TOKEN } from '../lib/consts';
import { GetKeyFunction } from '../lib/plugs';
import crypto from 'crypto';
import paseto from 'paseto';

const {
  V2: { decrypt, verify }
} = paseto;

const logReq = ({ path, hostname, body, params, query }: Request) => ({
  path,
  hostname,
  body,
  params,
  query
});

function verifyTokenExpiry(resiToken: ResiToken, req: Request, res: Response, next: NextFunction, logger?: Console) {
  if (!resiToken.expiry.getDay) {
    resiToken.expiry = new Date(resiToken.expiry);
  }
  if (resiToken.expiry > new Date()) return true;

  res.status(401);
  next('Token expired. Please login');
  logger?.warn('Resi Token Authorization: Expired token', { resiToken, ...logReq(req) });
  return false;
}

async function extractAndAddResiToken(
  req: Request | any,
  res: Response,
  next: NextFunction,
  publicKey: KeyObject,
  secret: KeyObject,
  logger?: Console
) {
  if (req.headers && req.headers.authorization) {
    const signedDecryptedToken = req.headers.authorization.replace('Bearer', '').trim();
    const token = await ResiToken.verifyDecryptToken(signedDecryptedToken, publicKey, secret);

    if (!verifyTokenExpiry(token, req, res, next, logger)) return false;

    Object.assign(req, { [RESI_TOKEN]: token });
    if (req[RESI_CONTEXT]) {
      Object.assign(req[RESI_CONTEXT], { token });
    }
    return true;
  } else {
    res.status(401);
    logger?.warn('Resi Token Authorization: No token in header', logReq(req));
    next('No authorization header');
    return false;
  }
}

export function makeAuthorizationMiddleware(publicKey: KeyObject, secret: KeyObject, logger?: Console) {
  const middleware = async function authorizationMiddleware(req: Request, res: Response, next: NextFunction) {
    try {
      const token = (req as any)[RESI_TOKEN] as ResiToken;
      if (token) {
        if (verifyTokenExpiry(token, req, res, next, logger)) {
          next();
        }
        return;
      } else {
        const success = await extractAndAddResiToken(req, res, next, publicKey, secret, logger);
        if (success) next();
        else {
          logger?.warn('Resi Token Authorization: invalid token', { ips: req.ips });
          next('Invalid token');
        }
      }
    } catch (e) {
      logger?.error('Resi Token Authorization: authorization exception', { error: e, ips: req.ips });
      next(e);
    }
  };

  return middleware;
}

export function makeRoleAuthorizationMiddleware(
  roles: number[],
  publicKey: KeyObject,
  secret: KeyObject,
  logger?: Console
) {
  const middleware = async function rolesMiddleware(req: Request, res: Response, next: NextFunction) {
    const reqAny = req as any;

    if (!reqAny[RESI_TOKEN]) {
      const success = await extractAndAddResiToken(req, res, next, publicKey, secret, logger);
      if (false === success) {
        return;
      }
    }

    const resiToken = reqAny[RESI_TOKEN] as ResiToken;

    if (!verifyTokenExpiry(resiToken, req, res, next, logger)) {
      return;
    }

    if (resiToken.role) {
      if (roles.includes(resiToken.role)) {
        next();
      } else {
        logger?.warn('Resi Role Authorization: wrong role', { resiToken, allowed: roles });
        res.status(401);
        next('Invalid role');
      }
    } else {
      logger?.warn('Resi Role Authorization: no role', { resiToken, allowed: roles });
      res.status(401);
      next('No role in token');
    }
  };

  return middleware;
}

export function addTokenToResponse<T>(target: T, token: string) {
  (target as any)[AUTH_TOKEN_FIELD] = token;
  return target;
}

export function makeVerifyPayloadMiddleware(getPublicKey: GetKeyFunction<any>, logger?: Console) {
  return async function (req: Request, res: Response, next: NextFunction) {
    try {
      let keystr = getPublicKey((req as any)[RESI_CONTEXT]);
      if (keystr instanceof Function) keystr = await keystr;

      const key = keystr
        ? crypto.createPublicKey({
            key: Buffer.from(keystr as string, 'base64'),
            format: 'der',
            type: 'spki'
          })
        : ((req as any)[RESI_CONTEXT].resiOptions.security.publicKey as crypto.KeyObject);

      const verified = await verify((req as any).body[BODY_FIELD_SIGNED] as string, key, { ignoreIat: true });
      req.body = verified;
      next();
    } catch (error) {
      logger?.warn('Resi Verify Payload: error', { error, body: req.body, token: (req as any)[RESI_TOKEN] });
      res.status(401);
      next('Invalid Request');
    }
  };
}

export function makeDecryptPayloadMiddleware(getSecretKey: GetKeyFunction<any>, logger?: Console) {
  return async function (req: Request, res: Response, next: NextFunction) {
    try {
      let keystr = getSecretKey((req as any)[RESI_CONTEXT]);
      if (keystr instanceof Function) keystr = await keystr;

      const key = keystr
        ? crypto.createSecretKey(Buffer.from(keystr as string))
        : ((req as any)[RESI_CONTEXT].resiOptions.security.secret as crypto.KeyObject);

      const decrypted = await decrypt((req as any).body[BODY_FIELD_ENCRYPTED] as string, key, { ignoreIat: true });
      req.body = decrypted;
      next();
    } catch (error) {
      logger?.warn('Resi Decrypt Payload: error', { error, body: req.body, token: (req as any)[RESI_TOKEN] });
      res.status(401);
      next('Invalid Request');
    }
  };
}

export { ResiToken, KeyFile, PasswordUtils };
